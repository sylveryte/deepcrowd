import cv2
import time
import mcnn_class
from PyQt5.QtCore import QThread
from fakeutils import FakeModel
from threading import Thread
from multiprocessing import Process
from cameras import AviCamera, AndCamera
from graphalAgent import Grapahl
import matplotlib.pyplot as plt
import logger
import alert_agent


"""
fake agents must have these fx
set_value_holder
run
show_feed
graphal
show_density
"""

def _startCam(url, name):
    camera = AndCamera(url)
    while True:
        img = camera.get_img()
        img = cv2.resize(img,(640,480))
        cv2.imshow(name, img)
        #To give the processor some less stress
        time.sleep(0.1)
        # Quit if q is pressed
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

def _startDensityCam(name):
    while True:
        img=cv2.imread(f"./temp/{name}.png")
        if img is not None:
            cv2.imshow(name, img)
        time.sleep(0.5)
        # Quit if q is pressed
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

def startCam(url,name):
    p = Process(target=_startCam,args=(url,name))
    p.start()

def startDensityCam(name):
    p = Process(target=_startDensityCam,args=(name,))
    p.start()


class FakeGuiAgent(QThread):
    #create the signal
    def __init__(self,lowerlimit, upperlimit, name, threshold):
        super(FakeGuiAgent, self).__init__(None)
        self.name = name 
        self.raw_name = name.replace(' ','')
        self.threshold = threshold
        self.value_holder = None
        self.logger = logger.Logger(name=self.raw_name)
        self.logger.start()
        self.model = FakeModel(lowerlimit, upperlimit)

    def set_value_holder(self, value_holder):
        self.value_holder = value_holder
        self.start()

    def graphal(self):
        Grapahl(self.name).show()
        print("did it worked")

    def run(self):
        while True:
            count = self.model.getcount()
            self.value_holder.value = count
            self.logger.log(count)

    def show_feed(self):
        print("Im fake agent no feed")

    def show_density(self):
        print("Im fake agent no density")



class MCNNGuiAgent(QThread):
    def __init__(self, camera, name, threshold):
        super(MCNNGuiAgent, self).__init__(None)
        self.name = name
        self.threshold = threshold
        self.raw_name = name.replace(' ','')
        self.camera = camera
        self.mcnn = mcnn_class.MCNN()
        self.value_holder = None
        self.showDensity = False
        self.logger = logger.Logger(name=self.raw_name)
        self.logger.start()
        self.alertAgent = alert_agent.AlertAgent()

    def run(self):
        while True:
            img = self.camera.get_img()
            img_mccn = self.mcnn.process_this_img(img)
            dmap, count = self.mcnn.cc(img_mccn)
            self.logger.log(count)
            if self.threshold < count:
                self.alertAgent.notify_auth(self.name,count,"over-crowded")
            self.value_holder.value = count
            time.sleep(1)
            if self.showDensity:
                plt.imshow(dmap[0][0])
                plt.savefig(f"./temp/{self.raw_name}.png")


    def set_value_holder(self, value_holder):
        self.value_holder = value_holder
        self.start()

    def show_feed(self):
        startCam(self.camera.raw_url, self.name)

    def show_density(self):
        self.showDensity = True
        startDensityCam(self.raw_name,)

    def graphal(self):
        GrapahlApp(self.name)
