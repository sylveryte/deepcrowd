import mcnn_class
import rcnn_class
import cameras
import time
import numpy as np
import cv2

if __name__ == "__main__":
    rcnn = rcnn_class.RCNN(vis=True)
    #camera = cameras.AviCamera(path="town.avi",gray=True)
    camera = cameras.AndCamera(url="192.168.43.26")

    winname = "DemoDeepCrowd"

    model = rcnn
    while True:
        cimg = camera.get_img()
        #print(cimg.shape)
        img = model.process_this_img(cimg)
        mimg, c = model.cc(img)
        
        print(c)
        #fi = np.hstack((img, mimg))

        cv2.imshow(winname, cimg)


        #To give the processor some less stress
        time.sleep(0.1)

        # Quit if q is pressed
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
