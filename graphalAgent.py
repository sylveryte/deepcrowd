import sys, os

from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QPushButton, QMenu, QHBoxLayout, QVBoxLayout, QListWidget, QLabel, QGridLayout, QSizePolicy

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib.dates as mdates

class PlotCanvas(FigureCanvas):
    
    def __init__(self, parent=None, width=5, height=4, dpi=100, name="noname",):
        self.name = name
        fig = Figure(figsize = (width, height), dpi=dpi)
        fig.autofmt_xdate()
        self.ax = fig.add_subplot(111)
        FigureCanvas.__init__(self, fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                QSizePolicy.Expanding,
                QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

    def clear_plot(self):
        self.ax.clear()
        self.draw()


    def plot(self,filename):
        data = pd.read_csv(filename,header=None,names=['time','amount'])
        data.time = pd.to_datetime(data['time'], format='%Y-%m-%d %H:%M:%S.%f')
        data.set_index(['time'],inplace=True)
        self.ax.plot(data.index, data.amount)
        self.ax.set_title(self.name)
        self.draw()

class Grapahl(QWidget):

    def __init__(self, name):
        super().__init__()
        self.name = name
        self.title = "DeepCrowd Graphal : "+name
        self.fillList()
        self.initUI()
    
    def fillList(self):
        self.logs = []
        for x in os.listdir('./logs'):
            if self.name in x:
                self.logs.append(x)

    def initUI(self):

        self.layout = QVBoxLayout()
        self.setLayout(self.layout)
        self.label = QLabel("file")
        self.layout.addWidget(self.label)
        self.optionLayout = QHBoxLayout()
        self.graphLayout = QHBoxLayout()
        self.actionLayout = QGridLayout()
        self.layout.addLayout(self.optionLayout,1)
        self.layout.addLayout(self.graphLayout,30)


        self.listw = QListWidget(self)
        for x in self.logs:
            self.listw.addItem(x)
            self.file = os.path.join('./logs/',x)
        self.listw.itemClicked.connect(self.listClicked)

        self.optionLayout.addWidget(self.listw)
        self.optionLayout.addLayout(self.actionLayout)

        #ActionsButtons
        plotbutton = QPushButton("Plot")
        plotbutton.clicked.connect(self.plotbutton)
        clear = QPushButton("Clear")
        clear.clicked.connect(self.clear)
        self.actionLayout.addWidget(plotbutton,0,0)
        self.actionLayout.addWidget(clear,0,1)

        self.plotCanvas = PlotCanvas(name=self.name)
        self.graphLayout.addWidget(self.plotCanvas)

        self.show()

    def listClicked(self, item):
        self.file = os.path.join("./logs/",item.text())
        self.label.setText(self.file)
    
    def plotbutton(self):
        self.plotCanvas.plot(self.file)

    def clear(self):
        self.plotCanvas.clear_plot()
    
    def wholeMonth(self):
        pass

class GrapahlApp(QMainWindow):
    def __init__(self,name):
        super().__init__()
        self.setWindowTitle("DeepCrowd : Graphal : "+name)
        self.setGeometry(100,100,1000,800)
        g = Grapahl(name)
        self.setCentralWidget(g)
        self.show()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    g = GrapahlApp("")
    sys.exit(app.exec_())
