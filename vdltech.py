import mcnn_class
import rcnn_class
import cameras
import time
import numpy as np
import cv2

import logger
import alert_agent

if __name__ == "__main__":
    mcnn = mcnn_class.MCNN(vis=True)
    #camera = cameras.AviCamera(path="town.avi",gray=True)
    camera = cameras.AndCamera(url="192.168.43.26",gray=True)
    logger = logger.Logger()
    alertAgent = alert_agent.AlertAgent()

    winname = "DemoDeepCrowd"
    count_warning_threshold = 10

    cv2.namedWindow(winname,cv2.WINDOW_AUTOSIZE)
    cv2.moveWindow(winname, 140, 150)
    model = mcnn
    logger.start()
    while True:
        cimg = camera.get_img()
        #print(cimg.shape)
        img = mcnn.process_this_img(cimg)
        mimg, c = model.cc(img)

        logger.log(c)
        if c > count_warning_threshold:
            alertAgent.notify_auth("deepcrowd",c,"debug-demo")
        
        #print(c)
        #fi = np.hstack((img, mimg))

        cv2.putText(img,str(c),(0,30),cv2.FONT_HERSHEY_SIMPLEX,1,(180,90,255),2)
        cv2.imshow(winname, cimg)


        #To give the processor some less stress
        time.sleep(0.1)

        # Quit if q is pressed
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
