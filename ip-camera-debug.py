from io import BytesIO

import cv2
import numpy
import requests
from PIL import Image

class CameraDebugger:
    def debug(self):
        print("Camera debugger in.")
        url_text = 'http://192.168.43.231:8080/photo.jpg' 
        while True:
            response = requests.get(url_text)
            pil_img = Image.open(BytesIO(response.content)).convert('RGB')
            open_cv_image = numpy.array(pil_img)
            # cv2.imshow('Camera num', open_cv_image)
            # Convert RGB to BGR
            final_img = open_cv_image[:, :, ::-1].copy()
            cv2.imshow('Camera Debugger', final_img)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        print("Camera debugger out.")


if __name__ == '__main__':
    CameraDebugger().debug()
