import numpy as np
import tensorflow as tf
import cv2
import time
import threading
import requests
from PIL import Image
from io import BytesIO


class DetectorAPI:
    def __init__(self, path_to_ckpt):
        self.path_to_ckpt = path_to_ckpt

        self.detection_graph = tf.Graph()
        with self.detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(self.path_to_ckpt, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')

        self.default_graph = self.detection_graph.as_default()
        self.sess = tf.Session(graph=self.detection_graph)

        # Definite input and output Tensors for detection_graph
        self.image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
        # Each box represents a part of the image where a particular object was detected.
        self.detection_boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
        # Each score represent how level of confidence for each of the objects.
        # Score is shown on the result image, together with the class label.
        self.detection_scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
        self.detection_classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
        self.num_detections = self.detection_graph.get_tensor_by_name('num_detections:0')

    def processFrame(self, image):
        # Expand dimensions since the trained_model expects images to have shape: [1, None, None, 3]
        image_np_expanded = np.expand_dims(image, axis=0)
        # Actual detection.
        start_time = time.time()
        (boxes, scores, classes, num) = self.sess.run(
            [self.detection_boxes, self.detection_scores, self.detection_classes, self.num_detections],
            feed_dict={self.image_tensor: image_np_expanded})
        end_time = time.time()

        # print("Elapsed Time:", end_time-start_time)

        im_height, im_width, _ = image.shape
        boxes_list = [None for i in range(boxes.shape[1])]
        for i in range(boxes.shape[1]):
            boxes_list[i] = (int(boxes[0, i, 0] * im_height),
                             int(boxes[0, i, 1] * im_width),
                             int(boxes[0, i, 2] * im_height),
                             int(boxes[0, i, 3] * im_width))

        return boxes_list, scores[0].tolist(), [int(x) for x in classes[0].tolist()], int(num[0])

    def close(self):
        self.sess.close()
        self.default_graph.close()

class RCNN:
    def __init__(self, model_path='./faster_rcnn_inception_v2_coco/frozen_inference_graph.pb', threshold = 0.7, vis=False):
        self.model_path = model_path
        self.threshold = threshold
        self.load_model()
        self.vis = vis

    def load_model(self):
        self.odapi = DetectorAPI(path_to_ckpt=self.model_path)
        print("RCNN model "+self.model_path+" loaded successfully")

    def cc(self, img):
        boxes, scores, classes, num = self.odapi.processFrame(img)
        count = 0

        for i in range(len(boxes)):
            # Class 1 represents human
            if classes[i] == 1 and scores[i] > self.threshold:
                count += 1
        if self.vis:
            self.display_img(img, boxes, count, classes, scores)
        return img, count

    def get_img_from_path(self, img_path):
        img = cv2.imread(img_path)
        #img = cv2.resize(img, (1280, 720))
        return img
        #return img.reshape(img.shape[0], img.shape[1], 3)

    def process_this_img(self, img):
        return img

    def display_img(self, img,  boxes, count, classes, scores):
        for i in range(len(boxes)):
            if classes[i] == 1 and scores[i] > self.threshold:
                box = boxes[i]
                cv2.rectangle(img, (box[1], box[0]), (box[3], box[2]), (255, 0, 0), 2)
        cv2.putText(img, str(count), (0, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (180, 90, 255), 2)
        cv2.imshow("RCNN", img)
        while True:
            k=cv2.waitKey(0)
            if k == ord('n'):
                break
        
if __name__ == "__main__":
#    rcnn = RCNN(threshold=0.2)
#
#    pic_dir_path = "test"
#    images_list = os.listdir(pic_dir_path)
#        for image_file_name in images_list:
#            image_file_path = os.path.join(pic_dir_path, image_file_name)
#            image_name = image_file_name.split(".")[-2]
#            #print(image_file_name,image_file_path,image_name,sep="\n")
#            print(image_file_name)
#
#            test_img(self.mcnn, image_file_path)
#            test_img(self.rcnn, image_file_path)
#

    print("no demo code :p")
    #model_path = './faster_rcnn_inception_v2_coco/frozen_inference_graph.pb'
    #odapi = DetectorAPI(path_to_ckpt=model_path)
    #threshold = 0.7
    #cap = cv2.VideoCapture('./town.avi')

    #count_warning_thresold = 10

    #logger = Logger.Logger()
    #alertAgent = AlertAgent.AlertAgent()
    #logger.start()

    #while True:
    #    r, img = cap.read()
    #    # img = cv2.resize(img, (1280, 720))
    #    img = cv2.resize(img, (640, 480))
    #    img = np.array(img)
    #    i = img
    #    if count > count_warning_thresold:
    #        alertAgent.notify_auth("avicam",count,"hi")
    #    logger.log(count)
    #    cv2.imshow("preview", fi)
    #    key = cv2.waitKey(1)
    #    if key & 0xFF == ord('q'):
    #        break
    #    if key == ord('p'):
    #        cv2.waitKey(0)
