from PyQt5.QtWidgets import QWidget, QHBoxLayout, QVBoxLayout, QPushButton, QLabel, QSizePolicy, QInputDialog, QLineEdit
from PyQt5.QtCore import  Qt
from PyQt5.QtGui import QPainter, QFont, QColor, QPen 
from math import ceil
from graphalAgent import GrapahlApp as Grapahl


def getText(self):
    text, okPressed = QInputDialog.getText(self, "Add Camera","Your ip:", QLineEdit.Normal, "")
    if okPressed and text != '':
        return text
    else:
        return None


class BurningWidget(QWidget):
    """
    This contains inner widget plus name and action buttons
    """
    def __init__(self, agent=None):
        super().__init__()
        self._threshold = agent.threshold
        self._name = agent.name
        self.agent = agent
        self.initUI()

    def initUI(self):
        self.setMaximumSize(1000,120)

        self.layout = QVBoxLayout()
        self.buttonLayout = QHBoxLayout()
        self.parentbuttonLayout = QHBoxLayout()
        self.burnLayout = QHBoxLayout()
        self.labelLayout = QHBoxLayout()
        self.layout.addLayout(self.labelLayout)
        self.layout.addLayout(self.parentbuttonLayout)
        self.layout.addLayout(self.burnLayout)
        self.setLayout(self.layout)
        
        #burn and burnlabel
        self.burnLabel = QLabel("25")
        self.burnLabel.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.burnLabel.setAlignment(Qt.AlignCenter)
        self.burn = Burn(self._threshold)
        self.burnLayout.addWidget(self.burnLabel,1)
        self.burnLayout.addWidget(self.burn,15)

        if not self.agent:
            print("No agent")
            self.close()

        #labels
        nameLabel = QLabel(self._name)
        self.labelLayout.addWidget(nameLabel)

        #buttons
        self.parentbuttonLayout.addWidget(QWidget(),1)
        self.parentbuttonLayout.addLayout(self.buttonLayout,15)
        self.graphAnalysis = QPushButton("Graph Analysis", self)
        self.showFeed = QPushButton("Show Feed", self)
        self.showDensity = QPushButton("Show Density Map", self)
        self.buttonLayout.addWidget(self.graphAnalysis,1)
        self.buttonLayout.addWidget(self.showFeed,1)
        self.buttonLayout.addWidget(self.showDensity,1)

        #buttonAsiignments
        self.showFeed.clicked.connect(self.agent.show_feed)
        self.showDensity.clicked.connect(self.agent.show_density)
        self.graphAnalysis.clicked.connect(self.graphal)

        #Agent Assignments
        self.agent.set_value_holder(self)

        self.show()

    def graphal(self):
        self.g=Grapahl(self._name)
        self.g.show()

    @property
    def value(self):
        return self.burn.value
    @value.setter
    def value(self, value):
        self.burnLabel.setText(str(int(value)))
        #print(f'heere {value}')
        self.burn.value = value

class Burn(QWidget):
    """
    This is inner widget
    """
    @staticmethod
    def give_range_of_tens(x):
        xn = x/10
        l = [0]
        y=0
        for _ in range(10):
            y+=xn
            l.append(ceil(y))
        return l

    def __init__(self, threshold):
        super().__init__()
        self._value = 0
        self._threshold = threshold
        self._maxcapacity = ceil(threshold*1.2)
        self.initUI()
    
    def initUI(self):
        self.setMinimumSize(1, 60)
        self.setMaximumSize(1000,80)
        self._num = Burn.give_range_of_tens(self._maxcapacity)

    def paintEvent(self, e):
        qp = QPainter()
        qp.begin(self)
        self.drawWidget(qp)
        qp.end()

    def drawWidget(self, qp):
        font = QFont('Serif', 7, QFont.Light)
        qp.setFont(font)

        size = self.size()
        w = size.width()
        h = size.height()
        #print(w,h)

        step = int(round(w / 11))


        till = int(((w / self._maxcapacity) * self._value))
        full = int(((w / self._threshold) * self._maxcapacity))

        #paint danger level
        rv=int((till/w)*255)
        qcolor = QColor(rv,255-rv,0)
        qp.setBrush(qcolor)
        qp.setPen(qcolor)
        qp.drawRect(0,0,till,h)

        #border
        pen = QPen(QColor(20, 20, 20), 1, 
            Qt.SolidLine)
        qp.setPen(pen)
        qp.setBrush(Qt.NoBrush)
        qp.drawRect(0, 0, w-1, h-1)

        #guidlines
        for i,x in zip(range(step, len(self._num)*step, step),self._num):
            qp.drawLine(i, 0, i, 14)
            metrics = qp.fontMetrics()
            fw = metrics.width(str(x))
            qp.drawText(i-fw/2, h/2, str(x))
        

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._value = value
        self.update()
