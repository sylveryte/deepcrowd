import numpy as np
import csv
import datetime
import multiprocessing
import threading
import time


class Logger:
    def __init__(self, interval=1,name=''):
        self.interval = interval
        self.queue = multiprocessing.Queue(maxsize=50)
        date_today = datetime.date.today()
        self.file_name = "logs/%s_%0.4d_%0.2d.csv" % (name,date_today.year, date_today.month)

    def log(self, count):
        self.queue.put(count, timeout=1)

    def timer(self):
        while (True):
            time.sleep(5)
            d = 0
            s = 0
            while not self.queue.empty():
                d += 1
                s += self.queue.get()
            if d:
                self.log_this(s / d)

    def log_this(self, avg_count):
        date = np.datetime_as_string(np.datetime64('now'))
        #print(date, avg_count)
        with open(self.file_name,'a') as csv_file:
            writer = csv.writer(csv_file)
            writer.writerow((date,avg_count))

    def start(self):
        threading.Thread(target=self.timer).start()

if __name__ == "__main__":
    logger = Logger()
    logger.log_this(21)
