import datetime
import sys
import time
import threading
import pyrebase


class AlertAgent:
    def __init__(self, cooltimeDown=5):
        config = {
            "apiKey": "AIzaSyDqi94LL7HwC44v4SpPOurUOGMv8fTETok",
            "authDomain": "deepcrowd-6cf9a.firebaseapp.com",
            "databaseURL": "https://deepcrowd-6cf9a.firebaseio.com",
            "projectId": "deepcrowd-6cf9a",
            "storageBucket": "deepcrowd-6cf9a.appspot.com",
            "messagingSenderId": "680063888719"
        }

        self.cooltimeDown = cooltimeDown

        self.cooltime = 0

        self.firebase = pyrebase.initialize_app(config)
        self.auth = self.firebase.auth()
        self.database = self.firebase.database()
        self.user = self.auth.sign_in_with_email_and_password("mr@man.com", "mrmanmr")
        self.token = self.user["idToken"]
        self.uid = self.user["localId"]
        
        threading.Thread(target=self.cooldown).start()
 

    def notify_auth(self, cname, count, comment):
        time=datetime.datetime.today()
        data = {
            'cameraname' : str(cname),
            'time': str(time),
            'count': str(count),
            'comment' : str(comment)
        }
        if(self.cooltime<1):
            self.get_compartment().push(data, self.get_token())
            self.cooltime += self.cooltimeDown
    def notify_auth_demo(self, cname, count, comment):
        print("demooing")
        time=datetime.date.today()
        data = {
            'cameraname' : str(cname),
            'time': str(time),
            'count': str(count),
            'comment' : str(comment)
        }
        self.cooltime += self.cooltimeDown
        print("done bro")


    def cooldown(self):
        while  True:
            time.sleep(1)
            if self.cooltime > 0:
                self.cooltime -= 1

    def get_token(self):
        if int(self.user['expiresIn']) < 60:
            self.token = self.auth.refresh(refresh_token=self.user['refreshToken'])['idToken']
        return self.token

    def get_compartment(self):
        return self.database.child("users").child(self.uid)


if __name__ == '__main__':
    if len(sys.argv)>1:
        m=sys.argv[1]
    else:
        m="douckk"
    a = AlertAgent()
    a.notify_auth("NorthWestCam2",45," ")
