from random import randint
import time

class FakeLogs:
    def __init__(self, seeds):
        self.seeds = seeds
        self.length = len(seeds)

    @property
    def fake_it(self):
        ix=0
        while True:
            i,s,e = self.seeds[ix]
            y = randint(s,e)
            time.sleep(1/(randint(2,5)))
            yield i,y
            ix+=1
            if ix == self.length:
                ix=0

class FakeModel:
    def __init__(self, lowerlimit,upperlimit):
        self.i=lowerlimit
        self.j=upperlimit
        self.step=int((self.j-self.i)/15)
        self.v = self.i

    def getcount(self):
        i = randint(0,1)
        if i:
            self.v+=self.step
        else:
            self.v-=self.step
        if self.v >self.j:
            self.v=self.j
        if self.v < self.i:
            self.v =  self.i
        time.sleep(0.3)
        return self.v
        

if __name__ == "__main__":
    seeds = [(1,15,39),(2,45,50),(3,100,120)]
    l = FakeLogs(seeds)
    #for _ in l.fake_it:
    #    print(_)
    f = FakeModel(4,19)
    for x in range(49):
        print(f.run())
