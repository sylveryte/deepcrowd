from PyQt5.QtWidgets import QWidget, QApplication, QHBoxLayout, QVBoxLayout, QPushButton, QLabel
from DeepCrowdWidgets import BurningWidget, getText
from DeepCrowdAgents import MCNNGuiAgent, FakeGuiAgent
from cameras import AviCamera, AndCamera
import sys 

class Example(QWidget):
    def __init__(self):
        super().__init__()
        self.title = "DeepCrowd"
        self.setGeometry(100,100,1000,300)
        self.initUI()

    def initUI(self):
        self.layout = QVBoxLayout()

        #close and hi button
        self.button_layout = QHBoxLayout()
        self.close_button = QPushButton("close")
        self.close_button.clicked.connect(self.close)
        self.add_button = QPushButton("add")
        self.button_layout.addWidget(self.close_button)
        self.button_layout.addWidget(self.add_button)
        self.layout.addLayout(self.button_layout)
        
        self.setLayout(self.layout)
        self.setWindowTitle("DeepCrowd")

        #mcnn camera widget adding 
        #self.layout.addWidget(BurningWidget(MCNNGuiAgent(AndCamera('192.168.43.26',gray=True), "Cam one",18)))
        #self.layout.addWidget(BurningWidget(MCNNGuiAgent(AndCamera('192.168.43.26',gray=True), "Cam fro",18)))
        #self.layout.addWidget(BurningWidget(MCNNGuiAgent(AndCamera('192.168.43.26',gray=True), "Cam two",18)))
        #self.layout.addWidget(BurningWidget(MCNNGuiAgent(AndCamera('192.168.43.26',gray=True), "Cam thr",18)))
        #fake cameras
        self.layout.addWidget(BurningWidget(FakeGuiAgent(4,35,"Cam1",35)))
        self.layout.addWidget(BurningWidget(FakeGuiAgent(10,67,"Cam2",68)))

        self.add_button.clicked.connect(self.addBurn)

        self.show()

    def addBurn(self):
        text = getText(self)
        ip, name, threshold = text.split()
        threshold = int(threshold)
        if text:
            self.layout.addWidget(BurningWidget(MCNNGuiAgent(AndCamera(ip,gray=True),name,threshold)))
            


if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
