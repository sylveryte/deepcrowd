import os
import sys
import torch
import numpy as np
import cv2

from crowd_count import CrowdCounter
import network
import utils
import matplotlib.pyplot as plt

class MCNN:

    def __init__(self, model_path='./shtechB_110_mcnn.h5', vis=False):
        self.model_path = model_path 
        self.load_model()
        self.vis = vis
    
    def load_model(self):
        torch.backends.cudnn.enabled = True
        torch.backends.cudnn.benchmark = False

        self.net = CrowdCounter()
              
        trained_model = os.path.join(self.model_path)
        network.load_net(trained_model, self.net)
        self.net.cuda()
        self.net.eval()
        print("MCNN : "+self.model_path+" loaded sucessfully.")

    def cc(self, img):
        density_map = self.net(img)
        density_map = density_map.data.cpu().numpy()
        count = np.sum(density_map)
        #print("Count : "+str(count)+" <"+img_name+">")
        #utils.save_density_map(density_map, self.output_dir, 'map_'+img_name+'.png')
        if self.vis:
            self.display_img(density_map, count)
        return density_map, count

    def display_results(self, input_img, gt_data, density_map):
        input_img = input_img[0][0]
        gt_data = 255*gt_data/np.max(gt_data)
        density_map = 255*density_map/np.max(density_map)
        density_map= density_map[0][0]
        if density_map.shape[1] != input_img.shape[1]:
             input_img = cv2.resize(input_img, (density_map.shape[1],density_map.shape[0]))
        result_img = np.hstack((input_img,density_map))
        result_img  = result_img.astype(np.uint8, copy=False)
        cv2.imshow('Result', result_img)

    def get_img_from_path(self, img_path):
        #print("fetching img "+img_path)
        img = cv2.imread(img_path,0)
        return self.process_this_img(img)
        
    def process_this_img(self, img):
        img = img.reshape((1,1,img.shape[0],img.shape[1]))
        return img

    def map_convert_displayable(self, img):
        pass


    def display_img(self, dmap, count):
        plt.imshow(dmap[0][0])
        plt.savefig("temp/j.png")
        img=cv2.imread("temp/j.png")
        cv2.putText(img, str(count), (0, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (180, 90, 255), 2)
        cv2.imshow("MCNN", img)

if __name__ == "__main__":
    pass
    #z,tp = sys.argv
    #tl = os.listdir(tp)
    #mcnn = MCNN()
    #for xn in tl:
    #    x=os.path.join(tp,xn)
    #    #print(x.split("/")[-1])
    #    x_name = x.split("/")[-1]
    #    img,count,dmap = mcnn.mcnn_this(x,x_name)
    #    mcnn.display_results(img,count,dmap)
    #    acimg=cv2.imread(x)
    #    cv2.putText(acimg,xn+" "+str(count),(0,30),cv2.FONT_HERSHEY_SIMPLEX,1,(180,90,255),2)
    #    cv2.imshow("actual image",acimg)
    #    k=cv2.waitKey(300)
    #    while(True):
    #        #print("here man")
    #        k=cv2.waitKey(0)
    #        if( k == ord('n') or k == ord('q')):
    #                break
    #    if k == ord('q'):
    #        cv2.destroyAllWindows()
    #        break
    #    cv2.waitKey(0)
    #mcnn_this(p,q);

