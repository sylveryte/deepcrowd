import os

import mcnn_class, rcnn_class

class DlTest:
    def __init__(self, pic_dir_path="test"):
        self.mcnn = mcnn_class.MCNN(vis=True)
        self.rcnn = rcnn_class.RCNN(threshold=0.5, vis=True)
        self.pic_dir_path = pic_dir_path

    def demo(self):
        images_list = os.listdir(self.pic_dir_path)
        for image_file_name in images_list:
            image_file_path = os.path.join(self.pic_dir_path, image_file_name)
            image_name = image_file_name.split(".")[-2]
            #print(image_file_name,image_file_path,image_name,sep="\n")
            print(image_file_name)
            
            self.test_img(self.mcnn, image_file_path)
            self.test_img(self.rcnn, image_file_path)


    def test_img(self, model, img_path):
        img = model.get_img_from_path(img_path)
        process_img, c = model.cc(img)
        print("count:"+str(c)+"\n")


if __name__ == "__main__":
    dt = DlTest()
    dt.demo()
