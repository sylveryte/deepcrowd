import cv2 
import requests
from PIL import Image
from io import BytesIO
import numpy as np
import time

class AviCamera:
    def __init__(self, path, gray=False, resize=False, W=640,H=480):
        self.path = path
        self.gray = gray
        self.resize = resize
        self.W = W
        self.H = H
        self.load()

    def load(self):
        self.cap = cv2.VideoCapture(self.path)
        print("Video loaded sucessfully")

    def get_img(self):
        ret, frame = self.cap.read()
        if self.resize:
            frame = cv2.resize(frame,(self.W,self.H))

        if self.gray:
            return cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        return frame


class AndCamera:
    def __init__(self, url, original=False, gray=False):
        self.raw_url=url
        self.url = 'http://'+url+':8080/shot.jpg'
        self.original = original;
        self.gray = gray

    def get_img(self):
        imgResp = requests.get(self.url)
        img = Image.open(BytesIO(imgResp.content))
        if self.gray:
            img = img.convert("L")
            return np.array(img)

        img = np.array(img)
        if self.original:
            return img
        #color correction
        img = img[:,:,::-1].copy()
        return img


if __name__ == "__main__":
    c = AndCamera("192.168.0.131")
    winname = "Debug : DeepCrowd"
    cv2.namedWindow(winname)
    cv2.moveWindow(winname, 50, 50)
    while True:
            # put the image on screen
        img = c.get_img()
        print(img.shape)
        cv2.imshow(winname,img)

        #To give the processor some less stress
        time.sleep(0.1) 

        # Quit if q is pressed
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
